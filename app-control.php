<?php
require_once('libraries/formvalidator.php');
require_once('app-model.php');

class App_control
{
    private $db;

    function __construct()
    {
        $this->db = new App_model(DB_USER, DB_PASS, DB_NAME, DB_HOST);
    }

    function get_user()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("uid", "req", "Please enter uid.");

            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                try {
                    $return = $this->db->get_user($_REQUEST["uid"]);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }


    function insert_user()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("email", "req", "Please enter email.");
            $validator->addValidation("password", "req", "Please enter password.");
            $validator->addValidation("firstname", "req", "Please enter firstname.");
            $validator->addValidation("lastname", "req", "Please enter lastname.");
            $validator->addValidation("position", "req", "Please enter position.");
            $validator->addValidation("department", "req", "Please enter department.");

            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                $data = array(
                    "email" => $_REQUEST["email"],
                    "password" => $_REQUEST["password"],
                    "firstname" => $_REQUEST["firstname"],
                    "lastname" => $_REQUEST["lastname"],
                    "position" => $_REQUEST["position"],
                    "department" => $_REQUEST["department"]
                );

                try {
                    $return = $this->db->insert_user("user", $data);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function update_user()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("uid", "req", "Please enter uid.");
            $validator->addValidation("email", "req", "Please enter email.");
            $validator->addValidation("password", "req", "Please enter password.");
            $validator->addValidation("firstname", "req", "Please enter firstname.");
            $validator->addValidation("lastname", "req", "Please enter lastname.");
            $validator->addValidation("position", "req", "Please enter position.");
            $validator->addValidation("department", "req", "Please enter department.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/

                $data = array(
                    "uid" => $_REQUEST["uid"],
                    "email" => $_REQUEST["email"],
                    "password" => $_REQUEST["password"],
                    "firstname" => $_REQUEST["firstname"],
                    "lastname" => $_REQUEST["lastname"],
                    "position" => $_REQUEST["position"],
                    "department" => $_REQUEST["department"]
                );

                $params = array(
                    "table" => "user",
                    "data" => $data,
                    "where" => "uid=%s",
                    "id" => $_REQUEST["uid"]
                );

                try {
                    $return = $this->db->update_user($params);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function delete_user()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("uid", "req", "Please enter uid.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                try {
                    $params = array(
                        "table" => "user",
                        "where" => "uid=%s",
                        "id" => $_REQUEST["uid"],
                    );
                    $this->db->delete_user($params);
                    $return = "ok";
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }

            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function get_monitoring()
    {
        $content = file_get_contents("files/" . $_REQUEST["method"] . ".json");
        return json_decode($content);
    }

    function get_monitoring_config()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("config_id", "req", "Please enter config_id.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                try {
                    $return = $this->db->get_monitoring_config($_REQUEST["config_id"]);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function insert_monitoring_config()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("uid", "req", "Please enter uid.");
            $validator->addValidation("monitor_id", "req", "Please enter monitor_id.");
            $validator->addValidation("expect_value", "req", "Please enter expect_value.");
            $validator->addValidation("actual_value", "req", "Please enter actual_value.");
            $validator->addValidation("yellow_value", "req", "Please enter yellow_value.");
            $validator->addValidation("red_value", "req", "Please enter red_value.");
            $validator->addValidation("alarm_mp3", "req", "Please enter alarm_mp3.");

            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                $data = array(
                    "uid" => $_REQUEST["uid"],
                    "monitor_id" => $_REQUEST["monitor_id"],
                    "expect_value" => $_REQUEST["expect_value"],
                    "actual_value" => $_REQUEST["actual_value"],
                    "yellow_value" => $_REQUEST["yellow_value"],
                    "red_value" => $_REQUEST["red_value"],
                    "alarm_mp3" => $_REQUEST["alarm_mp3"]
                );

                try {
                    $return = $this->db->insert_monitoring_config("monitoring_config", $data);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }


    function update_monitoring_config()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("config_id", "req", "Please enter config_id.");
            $validator->addValidation("uid", "req", "Please enter uid.");
            $validator->addValidation("monitor_id", "req", "Please enter monitor_id.");
            $validator->addValidation("expect_value", "req", "Please enter expect_value.");
            $validator->addValidation("actual_value", "req", "Please enter actual_value.");
            $validator->addValidation("yellow_value", "req", "Please enter yellow_value.");
            $validator->addValidation("red_value", "req", "Please enter red_value.");
            $validator->addValidation("alarm_mp3", "req", "Please enter alarm_mp3.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/

                $data = array(
                    "config_id" => $_REQUEST["config_id"],
                    "uid" => $_REQUEST["uid"],
                    "monitor_id" => $_REQUEST["monitor_id"],
                    "expect_value" => $_REQUEST["expect_value"],
                    "actual_value" => $_REQUEST["actual_value"],
                    "yellow_value" => $_REQUEST["yellow_value"],
                    "red_value" => $_REQUEST["red_value"],
                    "alarm_mp3" => $_REQUEST["alarm_mp3"]
                );

                $params = array(
                    "table" => "monitoring_config",
                    "data" => $data,
                    "where" => "config_id=%s",
                    "id" => $_REQUEST["config_id"]
                );

                try {
                    $return = $this->db->update_monitoring_config($params);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }


    function delete_monitoring_config()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("config_id", "req", "Please enter config_id.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                try {
                    $params = array(
                        "table" => "monitoring_config",
                        "where" => "config_id=%s",
                        "id" => $_REQUEST["config_id"],
                    );
                    $this->db->delete_monitoring_config($params);
                    $return = "ok";
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }

            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function get_monitoring_schedule()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("schedule_id", "req", "Please enter schedule_id.");
            $validator->addValidation("from_time", "req", "Please enter from_time.");
            $validator->addValidation("to_time", "req", "Please enter to_time.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                try {
                    $return = $this->db->get_monitoring_schedule($_REQUEST["schedule_id"]);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function insert_monitoring_schedule()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("config_id", "req", "Please enter config_id.");
            $validator->addValidation("from_time", "req", "Please enter from_time.");
            $validator->addValidation("to_time", "req", "Please enter to_time.");
            $validator->addValidation("status", "req", "Please enter status.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                $data = array(
                    "config_id" => $_REQUEST["config_id"],
                    "from_time" => $_REQUEST["from_time"],
                    "to_time" => $_REQUEST["to_time"],
                    "status" => $_REQUEST["status"]
                );

                try {
                    $return = $this->db->insert_monitoring_schedule("monitoring_schedule", $data);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function update_monitoring_schedule()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("schedule_id", "req", "Please enter schedule_id.");
            $validator->addValidation("config_id", "req", "Please enter config_id.");
            $validator->addValidation("from_time", "req", "Please enter from_time.");
            $validator->addValidation("to_time", "req", "Please enter to_time.");
            $validator->addValidation("status", "req", "Please enter status.");

            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/

                $data = array(
                    "schedule_id" => $_REQUEST["schedule_id"],
                    "config_id" => $_REQUEST["config_id"],
                    "from_time" => $_REQUEST["from_time"],
                    "to_time" => $_REQUEST["to_time"],
                    "status" => $_REQUEST["status"]

                );

                $params = array(
                    "table" => "monitoring_schedule",
                    "data" => $data,
                    "where" => "schedule_id=%s",
                    "id" => $_REQUEST["schedule_id"]
                );

                try {
                    $return = $this->db->update_monitoring_schedule($params);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }
            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

    function delete_monitoring_schedule()
    {
        if (count($_REQUEST) > 0) {
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("schedule_id", "req", "Please enter schedule_id.");
            /*Check invalid data*/
            if ($validator->ValidateForm()) {/*Valid*/
                try {
                    $params = array(
                        "table" => "monitoring_schedule",
                        "where" => "schedule_id=%s",
                        "id" => $_REQUEST["schedule_id"],
                    );
                    $this->db->delete_monitoring_schedule($params);
                    $return = "ok";
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code" => 500,
                        "message" => $e->getMessage()
                    );
                }

            } else {/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach ($error_hash as $inpname => $inp_err) {
                    $errMsg = $inp_err;
                    array_push($error_message, $errMsg);
                }
                $return["error"] = array("code" => 500, "message" => $error_message);
            }
        } else {
            $return["error"] = array("code" => 404, "message" => "Data not found.");
        }

        return $return;
    }

}


?>