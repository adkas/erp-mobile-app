<?php

class App_model
{
    function __construct($DB_USER, $DB_PASSWORD, $DB_NAME, $DB_SERVER)
    {
        /* DB::$user = $DB_USER;
         DB::$password = $DB_PASSWORD;
         DB::$dbName = $DB_NAME;
         DB::$host = $DB_SERVER;
         DB::query("SET NAMES UTF8");*/
        DB::$user = 'root';
        DB::$password = '';
        DB::$dbName = 'monitoring';
        DB::$host = 'localhost';
        DB::query("SET NAMES UTF8");
    }

    function insert($table, $data = array())
    {
        DB::insert($table, $data);
    }

    function update($params = array())
    {
        $table = $params["table"];
        $data = $params["data"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::update($table, $data, $where, $id);
    }

    function query($sql)
    {
        $query = DB::query($sql);
        return $query;
    }

    function get_user($uid)
    {
        $result = DB::query("SELECT * FROM user WHERE uid=" . $uid);
        return $result;
    }

    function insert_user($table, $data = array())
    {
        DB::insert($table, $data);
        $id = DB::insertId();
        $result = DB::query("SELECT * FROM user WHERE uid=" . $id);
        return $result;
    }


    function update_user($params = array())
    {
        $table = $params["table"];
        $data = $params["data"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::update($table, $data, $where, $id);
        $result = DB::query("SELECT * FROM user WHERE uid=" . $id);
        return $result;
    }

    function delete_user($params = array())
    {
        $table = $params["table"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::delete($table, $where, $id);
    }

    function get_monitoring_config($config_id)
    {
        $result = DB::query("SELECT * FROM monitoring_config WHERE config_id=" . $config_id);
        return $result;
    }

    function insert_monitoring_config($table, $data = array())
    {
        DB::insert($table, $data);
        $config_id = DB::insertId();
        $result = DB::query("SELECT * FROM monitoring_config WHERE config_id=" . $config_id);
        return $result;
    }

    function update_monitoring_config($params = array())
    {
        $table = $params["table"];
        $data = $params["data"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::update($table, $data, $where, $id);
        $result = DB::query("SELECT * FROM monitoring_config WHERE config_id=" . $id);
        return $result;
    }

    function delete_monitoring_config($params = array())
    {
        $table = $params["table"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::delete($table, $where, $id);
    }

    function get_monitoring_schedule($schedule_id)
    {
        $result = DB::query("SELECT * FROM monitoring_schedule WHERE schedule_id=" . $schedule_id);
        return $result;
    }

    function insert_monitoring_schedule($table, $data = array())
    {
        DB::insert($table, $data);
        $schedule_id = DB::insertId();
        $result = DB::query("SELECT * FROM monitoring_schedule WHERE schedule_id=" . $schedule_id);
        return $result;
    }

    function update_monitoring_schedule($params = array())
    {
        $table = $params["table"];
        $data = $params["data"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::update($table, $data, $where, $id);
        $result = DB::query("SELECT * FROM monitoring_schedule WHERE schedule_id=" . $id);
        return $result;
    }

    function delete_monitoring_schedule($params = array()){
        $table = $params["table"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::delete($table, $where,$id);
    }

}

?>