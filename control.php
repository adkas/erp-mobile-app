<?php
require_once('libraries/formvalidator.php');
require_once('model.php');
class Control{
    private $db;

    function __construct()
    {
        $this->db = new Model(DB_USER, DB_PASS, DB_NAME, DB_HOST);
    }

    function get(){
        if(count($_REQUEST)>0){
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("id","req","Please enter id.");
            /*Check invalid data*/
            if($validator->ValidateForm()){/*Valid*/
                try {
                    $return = $this->db->get_example($_REQUEST["id"]);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code"=>500,
                        "message"=>$e->getMessage()
                    );
                }
            }else{/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach($error_hash as $inpname => $inp_err)
                {
                    $errMsg =  $inp_err;
                    array_push($error_message,$errMsg);
                }
                $return["error"] = array("code"=>500,"message"=>$error_message);
            }
        }else{
            $return["error"] = array("code"=>404,"message"=>"Data not found.");
        }

        return $return;
    }

    function insert(){
        if(count($_REQUEST)>0){
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("name","req","Please enter name.");
            /*Check invalid data*/
            if($validator->ValidateForm()){/*Valid*/
                $data = array(
                    "name" =>$_REQUEST["name"]
                );

                try {
                    $return = $this->db->insert("test",$data);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code"=>500,
                        "message"=>$e->getMessage()
                    );
                }
            }else{/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach($error_hash as $inpname => $inp_err)
                {
                    $errMsg =  $inp_err;
                    array_push($error_message,$errMsg);
                }
                $return["error"] = array("code"=>500,"message"=>$error_message);
            }
        }else{
            $return["error"] = array("code"=>404,"message"=>"Data not found.");
        }

        return $return;
    }
    function update(){
        if(count($_REQUEST)>0){
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("id","req","Please enter id.");
            $validator->addValidation("name","req","Please enter name.");
            /*Check invalid data*/
            if($validator->ValidateForm()){/*Valid*/

                $data = array(
                    "name" =>$_REQUEST["name"]
                );

                $params = array(
                    "table" => "test",
                    "data" => $data,
                    "where" => "id=%s",
                    "id" => $_REQUEST["id"],
                );

                try {
                    $return = $this->db->update($params);
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code"=>500,
                        "message"=>$e->getMessage()
                    );
                }
            }else{/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach($error_hash as $inpname => $inp_err)
                {
                    $errMsg =  $inp_err;
                    array_push($error_message,$errMsg);
                }
                $return["error"] = array("code"=>500,"message"=>$error_message);
            }
        }else{
            $return["error"] = array("code"=>404,"message"=>"Data not found.");
        }

        return $return;
    }
    function delete(){
        if(count($_REQUEST)>0){
            /*Validate Data*/
            $validator = new FormValidator();
            $validator->addValidation("id","req","Please enter id.");
            /*Check invalid data*/
            if($validator->ValidateForm()){/*Valid*/
                try {
                    $params = array(
                        "table" => "test",
                        "where" => "id=%s",
                        "id" => $_REQUEST["id"],
                    );
                    $this->db->delete($params);
                    $return = "ok";
                } catch (Exception $e) {
                    $return["error"] = array(
                        "code"=>500,
                        "message"=>$e->getMessage()
                    );
                }

            }else{/*invalid*/
                /*Each error message*/
                $error_message = array();
                $error_hash = $validator->GetErrors();
                foreach($error_hash as $inpname => $inp_err)
                {
                    $errMsg =  $inp_err;
                    array_push($error_message,$errMsg);
                }
                $return["error"] = array("code"=>500,"message"=>$error_message);
            }
        }else{
            $return["error"] = array("code"=>404,"message"=>"Data not found.");
        }

        return $return;
    }
}
?>