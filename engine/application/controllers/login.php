<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (check_login()) {
            redirect(site_url('/home'));
        }
        $this->load->model('user_model','user');
    }
    public function index()
    {
        $this->load->view('login');
    }

    public function auth()
    {
        $param = array(
            'email'=>$this->input->post('email'),
            'password'=>$this->input->post('password')
        );
        $user = $this->user->login($param);
        if(!empty($user["uid"])){
            $login = array(
                'id'=>$user["uid"],
                'name'=>$user["firstname"].' '.$user["lastname"],
                'email'=>$user["email"]
            );
            auth_save_data($login);
            redirect(site_url('/home'));
        }else{
            redirect(site_url('/login'));
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */