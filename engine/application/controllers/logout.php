<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
    public function index()
    {
        logout();
        redirect(site_url('/login'));
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */