<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!check_login()) {
            redirect(site_url('/login'));
        }
    }

    public function index()
    {
        $this->load->view('purchase');
    }
}