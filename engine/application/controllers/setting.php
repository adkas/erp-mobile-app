<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!check_login()) {
            redirect(site_url('/login'));
        }
        $this->load->model('app_model','app');
    }

    public function index()
    {
        $this->load->view('setting');
    }

    public function purchase()
    {
        $this->load->view('setting-purchase-list');
    }

    public function purchase_form()
    {
        $this->load->view('setting-purchase-form');
    }

    public function manufacture()
    {
        $rs_step = $this->app->get_step();
        $data = array(
            "rs_step"=>$rs_step
        );
        $this->load->view('setting-manufacture-list',$data);
    }

    public function manufacture_form()
    {
        $step_id = $this->input->get('step_id');
        $rs_step = $this->app->get_step();
        $data = array(
            "rs_step"=>$rs_step,
            "step_id"=>$step_id
        );
        $this->load->view('setting-manufacture-form',$data);
    }

    public function material()
    {
        $rs_material = $this->app->get_material();
        $data = array(
            "rs_material"=>$rs_material
        );
        $this->load->view('setting-material-list',$data);
    }

    public function material_form()
    {
        $material_id = $this->input->get('material_id');
        $rs_material = $this->app->get_material();
        $data = array(
            "rs_material"=>$rs_material,
            "material_id"=>$material_id
        );
        $this->load->view('setting-material-form',$data);
    }

    public function product()
    {
        $this->load->view('setting-product-list');
    }

    public function product_form()
    {
        $this->load->view('setting-product-form');
    }

    public function schedule()
    {
        $schedule = $this->app->get_schedule();
        if(count($schedule)<=0){
            $default = $this->app->get_default_schedule();
            foreach($default as $row){
                $row["uid"] = $this->session->userdata('log_u_id');
                $this->db->insert('monitoring_schedule', $row);
            }
        }
        $schedule = $this->app->get_schedule();
        $data = array(
            "schedule"=>$schedule
        );
        $this->load->view('setting-schedule-list',$data);
    }

    public function schedule_form()
    {
        $schedule = $this->app->get_schedule();
        $data = array(
            "schedule"=>$schedule
        );
        $this->load->view('setting-schedule-form',$data);
    }
}