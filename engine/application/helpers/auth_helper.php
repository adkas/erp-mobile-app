<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @todo save data [member auth] ----> session cokie
 * @param $member
 * @param $auto_login
 * @return boolean
 */
function auth_save_data($user, $auto_login = 0)
{
    $ci = &get_instance();
    $sessiondata = array(
        'log_u_id' => $user['id'],
        'log_u_name' => $user['name'],
        'log_u_email' => $user['email']
    );
    if ($auto_login == 1) {
        // Change the config setting
        $time_cookie = 31536000;
        $ci->config->set_item('sess_expiration', $time_cookie);
        $ci->session->CI_Session();
    }

    return ($ci->session->set_userdata($sessiondata)) ? TRUE : FALSE;
}

function login_data($name = NULL)
{
    $ci = &get_instance();
    if ($ci->session->userdata('log_member_id')) {
        if ($name == NULL) {
            $data = array(
                'member_id' => $ci->session->userdata('log_member_id'),
                'name' => $ci->session->userdata('log_name'),
                'role' => $ci->session->userdata('log_role')
            );
        } else {
            $data = ($ci->session->userdata($name)) ? $ci->session->userdata($name) : FALSE;
        }
    } else {
        $data = FALSE;
    }
    return $data;
}

function check_login()
{
    $ci = &get_instance();
    return ($ci->session->userdata('log_u_id')) ? TRUE : FALSE;
}

function save_member_data($sess_info, $auto_login = 0)
{
    $ci = &get_instance();
    $sessiondata = array(
        'log_m_id' => $sess_info['id'],
        'log_m_email' => $sess_info['email'],
        'log_m_firstname' => $sess_info['firstname'],
        'log_m_lastname' => $sess_info['lastname']
    );
    if ($auto_login == 1) {
        // Change the config setting
        $time_cookie = 31536000;
        $ci->config->set_item('sess_expiration', $time_cookie);
        $ci->session->CI_Session();
    }

    return ($ci->session->set_userdata($sessiondata)) ? TRUE : FALSE;
}

function login($username, $password)
{
    $ci = &get_instance();
    $ci->load->model('user_model', 'user');
    $user = $ci->user->browse($username, $password);
    if (!empty($user['id'])) {
        $user['type'] = 'user';
        $data = array(
            'pass' => TRUE,
            'status' => 'ยินดีต้อนรับ',
            'rows' => $user
        );
        //$vArr["last_login"] = date("Y-m-d H:i:s");
        //$ci->user->update($vArr,$user['id']);
    } else {
        $data = array(
            'pass' => FALSE,
            'status' => '3',
            'rows' => array()
        );
    }

    return $data;
}

function logout()
{
    $ci = &get_instance();
    $sessiondata = array(
        'log_u_id' => NULL,
        'log_u_name' => NULL,
        'log_u_email' => NULL
    );

    $ci->session->unset_userdata($sessiondata);
}

function is_allowed($controller, $method)
{
    $ci = &get_instance();
    $acl = $ci->config->item('acl');
    $chk = 0;
    $log_u_id = $ci->session->userdata("log_m_id");
    foreach ($acl as $k => $val) {
        if (empty($log_u_id) and ($val['controller'] == $controller) and ($val['method'] == $method)) {
            $chk = 1;
        }
    }
    return ($chk == 1) ? true : false;
}

function is_login()
{
    $ci = &get_instance();
    return ($ci->session->userdata('log_m_id')) ? TRUE : FALSE;
}

?>