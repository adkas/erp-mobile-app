<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Common file helper
 *
 * @package CodeIgniter
 * @author  x3dev
 * @since  30/11/2010 18.00
 *
 */

/**
 * @todo show message debug
 */
if ( ! function_exists('alert'))
{
	function alert($str = array())
	{
		print "<pre>";
		print_r( $str );
		print "</pre>";
	}
}

if ( ! function_exists('debug'))
{
	function debug($str = array())
	{
		print "<pre>";
		print_r( $str );
		print "</pre>";
		die();
	}
}

/**
 * @todo convert len for utf8
 * @return length
 */
if ( ! function_exists('utf8_strlen'))
{
	function utf8_strlen($s) 
	{
		$c = strlen($s); $l = 0;
	    for ($i = 0; $i < $c; ++$i) if ((ord($s[$i]) & 0xC0) != 0x80) ++$l;
	    return $l;
	}
}

/**
 * @todo replace str for condition
 * @return string
 */
if ( ! function_exists('cleanurl'))
{
	function cleanurl($word) 
	{
		$newphrase = str_replace('/', '-', $word);
		$newphrase = str_replace('?', '-', $newphrase);
		$newphrase = str_replace(' ', '-', $newphrase);
		$newphrase = str_replace('%', '', $newphrase);
		return $newphrase;
	}
}

/**
 * @todo strip word
 * @return string
 */
if ( ! function_exists('strip_word'))
{
	function strip_words($word, $lchar=140, $replace="...") 
	{	
		$text = mb_substr(strip_tags(htmlspecialchars_decode($word)),0,$lchar,'UTF-8');
		if ((utf8_strlen($word)) > $lchar) 
		{
			$text = $text.$replace;
		}
		return $text;
	}
}

/**
 * @todo check ood
 * @return boolean
 */
if ( ! function_exists('isOdd'))
{
	function isOdd($number) {
		if ($number % 2 == 0) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}

/**
 * gen html tag pagination
 * @param $data=array pagination
 * @param $url
 * @param $css_style
 * @return html tag
 */
if ( ! function_exists('gen_html_pagination'))
{
	function gen_html_pagination($data=array(), $url='', $css_style='') 
	{
		$html = '';
		if ($data['row_count'] > 0 and $data['total_page'] > 1) 
		{
			if ($css_style != '') $css_style = ' style="'.$css_style.'"';
			$html.= '<ol id="pagination"'.$css_style.'>';
			if ($data['current_page'] > 1) {
				$back = $data['current_page']-1;
				//$html.= "<li><a href=\"".$url."/".$back."\">ถอย</a></li>";
				$html.= "<li><a href=\"".$url."page=1\">หน้าแรก</a></li>";
			}
			/*if ($data['current_page'] < $data['total_page']) {
				$next = $data['current_page']+1;
				$html.= "<li><a href=\"".$url."/".$next."\">Next</a></li>";
			}*/
			if ($data['start'] > 1) {
				$html.= "<li>&nbsp;...&nbsp;</li>";
			}
			for ($i=$data['start'];$i<=$data['end'];$i++) {
				if ($i == $data['current_page']) {
					$html.= "<li class=\"pagelink\">".$i."</li>";
				}
				else {
					$html.= "<li><a href=\"".$url."page=".$i."\">".$i."</a></li>";
				}
			}
			if ($data['end'] < $data['total_page']) {
				$html.= "<li>&nbsp;...&nbsp;</li>";
				$html.= "<li><a href=\"".$url."page=".$data['total_page']."\">หน้าสุดท้าย</a></li>";
			}	
			$html.= '</ol>';
		}
		
		return $html;
	}
}
if ( ! function_exists('shuffled'))
{
	function shuffled()
	{
		$str = 'abcdefghijklmnopqrstuvwxvzABCDEFGHIJKLMNOPQRSTUVWXVZ0123456789';
		$rsText = substr(str_shuffle($str),0,10);
		return $rsText;
	}
}
if (!function_exists('objectToArray')) {

    function objectToArray($d) {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            /*
             * Return array converted to object
             * Using __FUNCTION__ (Magic constant)
             * for recursive call
             */
            return array_map(__FUNCTION__, $d);
        } else {
            // Return array
            return $d;
        }
    }

}