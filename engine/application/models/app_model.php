<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class App_model extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_step()
    {
        $result = $this->db->select(array('step_id','step_name'))
            ->from('step')
            ->order_by('step_id', 'ASC')
            ->get()
            ->result_array();
        return $result;
    }

    function get_material()
    {
        $result = $this->db->select(array('material_id AS id','material_name AS name'))
            ->from('material_list')
            ->order_by('material_id', 'ASC')
            ->get()
            ->result_array();
        return $result;
    }

    function get_default_schedule()
    {
        $result = $this->db->select(array('from_time','to_time'))
            ->from('default_schedule')
            ->order_by('id', 'ASC')
            ->get()
            ->result_array();
        return $result;
    }

    function get_schedule()
    {
        $result = $this->db->select("*")
            ->from('monitoring_schedule')
            ->where('uid',$this->session->userdata('log_u_id'))
            ->where('status',"0")
            ->order_by('schedule_id', 'ASC')
            ->get()
            ->result_array();
        return $result;
    }


}