<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function login($param = array())
    {
        $email = $param["email"];
        $password = $param["password"];
        $result = $this->db->select('*')
            ->from('user')
            ->where('email',$email)
            ->where('password',md5($password))
            ->get()
            ->row_array();
        return $result;
    }

    function insert_entry()
    {
        $this->title = $_POST['title']; // please read the below note
        $this->content = $_POST['content'];
        $this->date = time();

        $this->db->insert('entries', $this);
    }

    function update_entry()
    {
        $this->title = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

}