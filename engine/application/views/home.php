<!DOCTYPE HTML>
<html>
<head><title>ERP - Monitoring</title>
    <base href="<?php echo base_url("assets");?>/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- FontAwesome - http://fortawesome.github.io/Font-Awesome/ -->
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <!-- jQueryMobileCSS - original without styling -->
    <link rel="stylesheet" href="css/jquery.mobile-1.4.2.min.css"/>
    <!-- nativeDroid core CSS -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.css"/>
    <!-- nativeDroid: Light/Dark -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.dark.css" id='jQMnDTheme'/>
    <!-- nativeDroid: Color Schemes -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.color.green.css" id='jQMnDColor'/>
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery / jQueryMobile Scripts -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>
<div data-role="page" data-theme='b'>
    <div data-role="panel" data-display="push" id="mypanel" data-theme="b">
        <ul data-role="listview">
            <li>Menu</li>
            <li data-icon='false'><a href="<?php echo site_url('/setting')?>" data-ajax="true"><i class='lIcon fa fa-cog'></i> Setting</a></li>
            <li data-icon='false'><a href="<?php echo site_url('/logout')?>" data-ajax="false"><i class='lIcon fa fa-sign-out'></i> Logout</a></li>
        </ul>
    </div>
    <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'><h1>Home</h1> <a href="#mypanel" data-role='button' data-inline='true'><i class='fa fa-home'></i></a></div>
    <div data-role="content">
        <ul data-role="listview" data-inset="false" data-icon="false" data-divider-theme="b">
            <!--<li data-role="list-divider">Demos</li>-->
            <li><a href="<?php echo site_url('/purchase')?>" data-ajax="true"><i class='fa fa-shopping-cart'></i> Sale Order <span
                        style="float: right; display: inline;"><img src="images/green.png"
                                                                    align="absmiddle"> </span></a></li>
            <li><a href="<?php echo site_url('/manufacture')?>" data-ajax="true"><i class='fa fa-gears'></i> Manufacturing <span
                        style="float: right; display: inline;"><img src="images/yellow.png"
                                                                    align="absmiddle"> </span></a></li>
            <!-- <li>                <a href="tables.html" data-ajax="true"><i class='fa fa-table'></i> Tables</a>            </li>-->
            <li><a href="<?php echo site_url('/material')?>" data-ajax="true"><i class='fa fa-cube'></i> Material Remain <span
                        style="float: right; display: inline;"><img src="images/red.png" align="absmiddle"> </span></a>
            </li>
            <li><a href="<?php echo site_url('/product')?>" data-ajax="true"><i class='fa fa-cubes'></i> Product Remain <span
                        style="float: right; display: inline;"><img src="images/grey.png" align="absmiddle"> </span></a>
            </li>
            <!--<li >                <a href="headerfooter.html" data-ajax="false"><i class='fa fa-angle-down'></i> Header &amp; Footer</a>            </li>            <li>                <a href="tables.html" data-ajax="false"><i class='fa fa-table'></i> Tables</a>            </li>            <li>                <a href="dialogsnpopups.html" data-ajax="false"><i class='fa fa-external-link'></i> Dialogs &amp; Popups</a>            </li>            <li>                <a href="panels.html" data-ajax="false"><i class='fa fa-tasks'></i> Panels</a>            </li>            <li>                <a href="autocomplete.html" data-ajax="false"><i class='fa fa-filter'></i> Autocomplete</a>            </li>            <li>                <a href="collapsible.html" data-ajax="false"><i class='fa fa-chevron-up'></i> Collapsible</a>            </li>            <li>                <a href="accordions.html" data-ajax="false"><i class='fa fa-chevron-right'></i> Accordions</a>            </li>            <li>                <a href="listviews.html" data-ajax="false"><i class='fa fa-list'></i> Listviews</a>            </li>            <li>                <a href="forms.html" data-ajax="false"><i class='fa fa-external-link'></i> Forms</a>            </li>-->
        </ul>
    </div>
</div>
<script src="js/nativedroid.script.js"></script>
</body>
</html>