<!DOCTYPE html>

<html lang='en'>

<head>

    <meta charset="UTF-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>

        ERP Monitoring

    </title>
    <base href="<?php echo base_url();?>">
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/login.css"/>

    <script type="text/javascript">

        /*jQuery(document).on('click', 'input[type="submit"]', function (e) {

         e.preventDefault();

         var btn = jQuery(this);



         btn.button('loading');

         jQuery('#unit_test').ajaxSubmit({

         success: function (data) {

         console.log(data);

         var json = $.parseJSON(data);

         jQuery('#json_result').html(JSON.stringify(json, undefined, 2));



         }

         });

         btn.button('reset');

         window.location.href = "home.html";

         });*/

        $(document).ready(function () {

            $("#btnSubmit").click(function () {

                $("#WebForm").submit();

            });

        });

    </script>

</head>

<body>



<form method="post" action="<?php echo site_url('/login/auth');?>" id="WebForm">

    <h1>Login to ERP Monitoring</h1>



    <div class="inset">

        <p>

            <label for="email">EMAIL</label>

            <input type="text" name="email" id="email">

        </p>



        <p>

            <label for="password">PASSWORD</label>

            <input type="password" name="password" id="password">

        </p>

    </div>

    <p class="p-container">

        <input type="button" name="go" id="btnSubmit" value="Log in">

    </p>

</form>



</body>

</html>

