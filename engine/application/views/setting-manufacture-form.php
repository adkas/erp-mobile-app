<!DOCTYPE HTML>
<html>
<head><title>ERP - Monitoring</title>
    <base href="<?php echo base_url("assets");?>/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- FontAwesome - http://fortawesome.github.io/Font-Awesome/ -->
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <!-- jQueryMobileCSS - original without styling -->
    <link rel="stylesheet" href="css/jquery.mobile-1.4.2.min.css"/>
    <!-- nativeDroid core CSS -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.css"/>
    <!-- nativeDroid: Light/Dark -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.dark.css" id='jQMnDTheme'/>
    <!-- nativeDroid: Color Schemes -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.color.green.css" id='jQMnDColor'/>
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery / jQueryMobile Scripts -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>
<div data-role="page" data-theme='b'>
    <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
        <a href="<?php echo site_url('/setting/manufacture')?>" data-ajax="true"><i class='fa fa-bars'></i></a>
        <h1>Setting Manufacture</h1>
    </div>
    <div data-role="content">
        <form>
            <ul data-role="listview" data-inset="true">
                <li data-role="fieldcontain">
                    <label for="select-choice-1b" class="select">Step</label>
                    <select name="step_id" id="step_id" data-native-menu="true">
                        <?php foreach($rs_step as $row){?>
                            <option value="<?php echo $row["step_id"];?>" <?php if($row["step_id"]==$step_id){?> selected<?php } ?>><?php echo $row["step_name"];?></option>
                        <?php } ?>
                    </select>
                </li>
                <li data-role="fieldcontain">
                    <label for="select-choice-1b" class="select">Product</label>
                    <select name="product_id" id="product_id" data-native-menu="true">
                        <option value="1">Rambutan in Syrup</option>
                    </select>
                </li>
                <li data-role="fieldcontain">
                    <label for="excpect_value">Expect Value:</label>
                    <input type="text" name="excpect_value" id="excpect_value" value="" data-clear-btn="true">
                </li>

                <li data-role="fieldcontain">
                    <label for="yellow_value">Yellow Value:</label>
                    <input type="text" name="yellow_value" id="yellow_value" value="" data-clear-btn="true">
                </li>

                <li data-role="fieldcontain">
                    <label for="red_value">Red Value:</label>
                    <input type="text" name="red_value" id="red_value" value="" data-clear-btn="true">
                </li>

                <li data-role="fieldcontain">
                    <label for="unit">Unit:</label>
                    <input type="text" name="unit" id="unit" value="" data-clear-btn="true">
                </li>

                <li>
                    <button type="submit" data-inline='true'><i class='lIcon fa fa-floppy-o'></i>Save</button>
                    <button type="reset" data-inline='true'><i class='lIcon fa fa-times'></i>Cancel</button>
                </li>
            </ul>
        </form>
    </div>
</div>
<script src="js/nativedroid.script.js"></script>
</body>
</html>