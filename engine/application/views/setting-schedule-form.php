<!DOCTYPE HTML>
<html>
<head><title>ERP - Monitoring</title>
    <base href="<?php echo base_url("assets");?>/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- FontAwesome - http://fortawesome.github.io/Font-Awesome/ -->
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <!-- jQueryMobileCSS - original without styling -->
    <link rel="stylesheet" href="css/jquery.mobile-1.4.2.min.css"/>
    <!-- nativeDroid core CSS -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.css"/>
    <!-- nativeDroid: Light/Dark -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.dark.css" id='jQMnDTheme'/>
    <!-- nativeDroid: Color Schemes -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.color.green.css" id='jQMnDColor'/>
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery / jQueryMobile Scripts -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.mobile-1.4.2.min.js"></script>

</head>
<body>
<div data-role="page" data-theme='b'>
    <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
        <a href="<?php echo site_url('/setting/schedule')?>" data-ajax="true"><i class='fa fa-bars'></i></a>
        <h1>Setting Schedule</h1>
    </div>
    <div data-role="content">
        <form>
            <ul data-role="listview" data-inset="true">

                <li data-role="fieldcontain">
                    <label for="from_time" class="select">From Time</label>
                    <select name="from_time" id="from_time" data-native-menu="true">
                        <?php foreach($schedule as $row){?>
                            <option value="<?php echo $row["from_time"];?>"><?php echo $row["from_time"];?></option>
                        <?php } ?>
                    </select>
                </li>

                <li data-role="fieldcontain">
                    <label for="to_time" class="select">To Time</label>
                    <select name="to_time" id="to_time" data-native-menu="true">
                        <?php foreach($schedule as $row){?>
                            <option value="<?php echo $row["to_time"];?>"><?php echo $row["to_time"];?></option>
                        <?php } ?>
                    </select>
                </li>

                <li data-role="fieldcontain">
                    <label for="active" class="select">Active</label>
                    <select name="active" id="active" data-native-menu="true">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </li>

                <li>
                    <button type="submit" data-inline='true'><i class='lIcon fa fa-floppy-o'></i>Save</button>
                    <button type="reset" data-inline='true'><i class='lIcon fa fa-times'></i>Cancel</button>
                </li>
            </ul>
        </form>
    </div>
</div>
<script src="js/nativedroid.script.js"></script>
</body>
</html>