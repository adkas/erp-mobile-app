<!DOCTYPE HTML>
<html>
<head><title>ERP - Monitoring</title>
    <base href="<?php echo base_url("assets");?>/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- FontAwesome - http://fortawesome.github.io/Font-Awesome/ -->
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <!-- jQueryMobileCSS - original without styling -->
    <link rel="stylesheet" href="css/jquery.mobile-1.4.2.min.css"/>
    <!-- nativeDroid core CSS -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.css"/>
    <!-- nativeDroid: Light/Dark -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.dark.css" id='jQMnDTheme'/>
    <!-- nativeDroid: Color Schemes -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.color.green.css" id='jQMnDColor'/>
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery / jQueryMobile Scripts -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>
<div data-role="page" data-theme='b'>
    <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
        <a href="<?php echo site_url('/setting')?>" data-ajax="true"><i class='fa fa-bars'></i></a>
        <h1>Setting Schedule</h1>
    </div>
    <div data-role="content">


        <div class='inset'>

            <table data-role="table" id="table-column-toggle" data-mode="columntoggle" class="ui-responsive table-stroke">
                <thead>
                <tr>
                    <th data-priority="1"><abbr title="No">No</abbr></th>
                    <th>Schedule</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($schedule as $row){
                    ?>
                    <tr>
                        <td><?php echo $no++;?></td>
                        <td><?php echo $row["from_time"].' - '.$row["to_time"];?></td>
                        <td><a href="<?php echo site_url('/setting/schedule_form?schedule_id='.$row["schedule_id"]);?>" data-ajax="true">Edit</a></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>

    </div>
</div>
<script src="js/nativedroid.script.js"></script>
</body>
</html>