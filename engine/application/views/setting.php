<!DOCTYPE HTML>
<html>
<head><title>ERP - Monitoring</title>
    <base href="<?php echo base_url("assets");?>/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <!-- FontAwesome - http://fortawesome.github.io/Font-Awesome/ -->
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <!-- jQueryMobileCSS - original without styling -->
    <link rel="stylesheet" href="css/jquery.mobile-1.4.2.min.css"/>
    <!-- nativeDroid core CSS -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.css"/>
    <!-- nativeDroid: Light/Dark -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.dark.css" id='jQMnDTheme'/>
    <!-- nativeDroid: Color Schemes -->
    <link rel="stylesheet" href="css/jquerymobile.nativedroid.color.green.css" id='jQMnDColor'/>
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery / jQueryMobile Scripts -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>
<div data-role="page" data-theme='b'>
    <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>
        <a href="<?php echo site_url('/home')?>" data-ajax="true"><i class='fa fa-bars'></i></a>
        <h1>Setting</h1>
    </div>
    <div data-role="content">
        <ul data-role="listview" data-inset="false" data-icon="false" data-divider-theme="b">
            <li><a href="<?php echo site_url('/setting/purchase')?>" data-ajax="true"><i class='fa fa-shopping-cart'></i> Setting Sale Order </a></li>
            <li><a href="<?php echo site_url('/setting/manufacture')?>" data-ajax="true"><i class='fa fa-cogs'></i> Setting Manufacturing</a></li>
            <li><a href="<?php echo site_url('/setting/material')?>" data-ajax="true"><i class='fa fa-cube'></i> Setting Material Remain </a>
            </li>
            <li><a href="<?php echo site_url('/setting/product')?>" data-ajax="true"><i class='fa fa-cubes'></i> Setting Product Remain </a>
            </li>
            <li><a href="<?php echo site_url('/setting/schedule')?>" data-ajax="true"><i class='fa fa-clock-o'></i> Setting Schedule </a>
            </li>
        </ul>
    </div>
</div>
<script src="js/nativedroid.script.js"></script>
</body>
</html>