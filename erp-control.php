<?php
require_once('libraries/formvalidator.php');
require_once('erp-model.php');
class Erp_control{
	private $db;
	
	function __construct()
    {
        $this->db = new Erp_model(DB_USER, DB_PASS, DB_NAME, DB_HOST);
    }
	
	function get_user(){
		$result = $this->db->query("SELECT * FROM user");
		$return = array("users"=>$result);
		return $return;
	}

	function get_manufacture_value(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("from","req","Please enter time from.");
			$validator->addValidation("to","req","Please enter time to.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$result = $this->db->get_manufacture_value($_REQUEST["from"],$_REQUEST["to"]);
				$return["result"] = array(
					"manfacture_value"=>$result[0]["manfacture_value"],
					"from"=>$_REQUEST["from"],
					"to"=>$_REQUEST["to"]
				);
				#$return["from"]=$_REQUEST["from"];
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_material_remain(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("material_id","req","Please enter material_id.");

			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$return = json_decode(file_get_contents("files/".$_REQUEST["method"].".json"));
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_product_boiling_qty(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("step","req","Please enter step.");
			$validator->addValidation("from","req","Please enter time from.");
			$validator->addValidation("to","req","Please enter time to.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$return = json_decode(file_get_contents("files/".$_REQUEST["method"].".json"));
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_product_cleaning_qty(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("step","req","Please enter step.");
			$validator->addValidation("from","req","Please enter time from.");
			$validator->addValidation("to","req","Please enter time to.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$return = json_decode(file_get_contents("files/".$_REQUEST["method"].".json"));
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_product_cutting_qty(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("step","req","Please enter step.");
			$validator->addValidation("from","req","Please enter time from.");
			$validator->addValidation("to","req","Please enter time to.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$return = json_decode(file_get_contents("files/".$_REQUEST["method"].".json"));
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_product_packing_qty(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("step","req","Please enter step.");
			$validator->addValidation("from","req","Please enter time from.");
			$validator->addValidation("to","req","Please enter time to.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$return = json_decode(file_get_contents("files/".$_REQUEST["method"].".json"));
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_product_putting_qty(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("step","req","Please enter step.");
			$validator->addValidation("from","req","Please enter time from.");
			$validator->addValidation("to","req","Please enter time to.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$return = json_decode(file_get_contents("files/".$_REQUEST["method"].".json"));
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_product_remain(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("product_id","req","Please enter product_id.");

			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$return = json_decode(file_get_contents("files/".$_REQUEST["method"].".json"));
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function get_purchase_value(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			/*Define Rule*/
			$validator->addValidation("username","req","Please enter username.");
			$validator->addValidation("password","req","Please enter password.");
			$validator->addValidation("from","req","Please enter time from.");
			$validator->addValidation("to","req","Please enter time to.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$result = $this->db->get_purchase_value($_REQUEST["from"],$_REQUEST["to"]);
				$return["result"] = array(
					"purchase_value"=>$result[0]["purchase_value"],
					"from"=>$_REQUEST["from"],
					"to"=>$_REQUEST["to"]
				);
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
	function insert_purchase(){
		if(count($_REQUEST)>0){
			/*Validate Data*/
			$validator = new FormValidator();
			$validator->addValidation("product_id","req","Please enter product id.");
			$validator->addValidation("quantity","req","Please enter quantity.");
			/*Check invalid data*/
			if($validator->ValidateForm()){/*Valid*/
				$data = array(
					"product_id" =>$_REQUEST["product_id"],
					"quantity" =>$_REQUEST["quantity"]
				);

				try {
					$return = $this->db->insert_data("purchase_list",$data);
				} catch (Exception $e) {
					$return["error"] = array(
						"code"=>500,
						"message"=>$e->getMessage()
					);
				}
			}else{/*invalid*/
				/*Each error message*/
				$error_message = array();
				$error_hash = $validator->GetErrors();
				foreach($error_hash as $inpname => $inp_err)
				{
					$errMsg =  $inp_err;
					array_push($error_message,$errMsg);
				}
				$return["error"] = array("code"=>500,"message"=>$error_message);
			}
		}else{
			$return["error"] = array("code"=>404,"message"=>"Data not found.");
		}

		return $return;
	}
}
?>