<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sakda
 * Date: 3/12/2015
 * Time: 4:56 PM
 */

$config["get_purchase_value"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_purchase_value'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'from' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 09:00:00"),
    ),
    'to' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 10:00:00"),
    )
);

$config["get_manufacture_value"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_manufacture_value'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'from' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 09:00:00"),
    ),
    'to' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 10:00:00"),
    )
);

$config["get_material_remain"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_material_remain'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'material_id' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'M051',
        'require' => 1
    )
);

$config["get_product_remain"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_product_remain'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'product_id' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 1
    )
);

$config["get_product_cleaning_qty"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_product_cleaning_qty'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'step' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '01',
        'require' => 1
    ),
    'from' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 09:00:00"),
    ),
    'to' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 10:00:00"),
    )
);

$config["get_product_cutting_qty"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_product_cutting_qty'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'step' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '02',
        'require' => 1
    ),
    'from' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 09:00:00"),
    ),
    'to' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 10:00:00"),
    )
);

$config["get_product_boiling_qty"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_product_boiling_qty'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'step' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '03',
        'require' => 1
    ),
    'from' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 09:00:00"),
    ),
    'to' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 10:00:00"),
    )
);

$config["get_product_putting_qty"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_product_putting_qty'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'step' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '04',
        'require' => 1
    ),
    'from' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 09:00:00"),
    ),
    'to' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 10:00:00"),
    )
);

$config["get_product_packing_qty"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get_product_packing_qty'
    ),
    'username' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'ru1'
    ),
    'password' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1234',
        'require' => 1
    ),
    'step' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '05',
        'require' => 1
    ),
    'from' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 09:00:00"),
    ),
    'to' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => date("Y-m-d 10:00:00"),
    )
);

$config["insert_purchase"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'insert_purchase'
    ),
    'product_id' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => '1'
    ),
    'quantity' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 1234,
        'require' => 1
    )

);

$output = array(
    'field' => $config[$_REQUEST["method"]],
);
echo json_encode($output);