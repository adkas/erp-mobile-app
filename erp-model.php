<?php
class Erp_model
{
    function __construct($DB_USER, $DB_PASSWORD, $DB_NAME, $DB_SERVER)
    {
        DB::$user = $DB_USER;
        DB::$password = $DB_PASSWORD;
        DB::$dbName = $DB_NAME;
        DB::$host = $DB_SERVER;
        DB::query("SET NAMES UTF8");
    }
	
	function insert($table,$data = array())
    {
        DB::insert($table, $data);
    }
	
	function update($params = array())
    {
        $table = $params["table"];
		$data = $params["data"];
		$where = $params["where"]; //column=%s
		$id = $params["id"];
		DB::update($table, $data, $where, $id);
        return $id;
    }
	
	function query($sql){
		$query = DB::query($sql);
        return $query;
	}
    function get_manufacture_value($from,$to){
        $sql = 'SELECT quantity AS manfacture_value FROM manufacture_list WHERE manufacture_date BETWEEN "'.$from.'" AND "'.$to.'"';
        $result = DB::query($sql); /*to do*/
        return $result;
    }
    function get_purchase_value($from,$to){
        $sql = 'SELECT quantity AS purchase_value FROM purchase_list WHERE purchase_date BETWEEN "'.$from.'" AND "'.$to.'"';
        $result = DB::query($sql); /*to do*/
        return $result;
    }

    function get_product_remain($from,$to){
        $result = DB::query("SELECT * FROM get_product_remain WHERE FROM =".$from); /*to do*/
        $result = DB::query("SELECT * FROM get_product_remain WHERE FROM =".$to); /*to do*/
        return $result;
    }
    function insert_data($table,$data = array())
    {
        DB::insert($table, $data);
        $id = DB::insertId();
        return $id;
    }
}
?>