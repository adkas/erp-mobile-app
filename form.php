<!DOCTYPE html>
<html lang="en">
<head>
    <title>Application Monitoring Console</title>
    <meta charset="UTF-8">
    <meta name=description content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="assets/css/bootstrap-reset.css" rel="stylesheet" media="screen">
    <link href="assets/css/style.css" rel="stylesheet" media="screen">

    <!-- jQuery -->
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <!-- Bootstrap JavaScript -->

    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.form.min.js"></script>

    <script type="text/javascript">
        jQuery(document).on('click', 'button[type="submit"]', function (e) {
            e.preventDefault();
            var btn = jQuery(this);

            btn.button('loading');
            jQuery('#unit_test').ajaxSubmit({
                success: function (data) {
                    console.log(data);
                    var json = $.parseJSON(data);
                    jQuery('#json_result').html(JSON.stringify(json, undefined, 2));

                }
            });
            btn.button('reset');
        });
        /*END OF FORM SUBMIT*/


        jQuery(document).ready(function ($) {

            $('a.list-group-item').on('click', function (e) {
                e.preventDefault();
                if ($(this).hasClass('active')) {
                    return false;
                }
                $(this).addClass('active');
                $(this).siblings().removeClass('active');
                $('pre').html('');
                var _method = $(this).attr('rel');

                $('#className>span').html($(this).text());
                $.ajax({
                    url: 'input.php',
                    data: {method: _method},
                    error: function (data) {
                        console.log(data);
                        $('[data-status]').removeAttr('class').addClass('label label-danger').html(data.status + '  ' + data.statusText)
                    },
                    success: function (data) {

                        $('[data-status]').removeAttr('class').addClass('label label-success').html('success');
                        var json = $.parseJSON(data);
                        console.log(json);
                        if (json.length != 0) {

                            var html = '';
                            $.each(json.field, function (index, elem) {
                                html += '<div class="form-group">';


                                var required = '';
                                if (elem.require == 1) {
                                    required = 'required'
                                }

                                if (elem.type != 'hidden')
                                    html += '<label>' + index + '</label>'; // Print Label

                                if (elem.type == 'select') {
                                    // if this is select element
                                }else if (elem.type == 'textarea') {
                                    html += '<textarea class="form-control" name="' + index + '" rows="5">' + elem.value + '</textarea>';
                                }  else {
                                    html += '<input type="' + elem.type + '" class="form-control" name="' + index + '" ' + required + '  placeholder="' + elem.placeholder + '" value="' + elem.value + '">';
                                }
                                html += '</div>';
                            });
                            html +=
                                '<div class="form-group">' +
                                '<button class="btn btn-primary btn-block" data-loading-text="Loading..." type="submit">Submit</button>' +
                                '</div>';
                            $('form#unit_test').find('.custom-field').html('').append(html);

                        }
                    }
                })

            });
        });
    </script>
</head>
<body style="background: #f1f2f7">
<div class="container">
    <div class="row">

        <div class="page-header">
            <h1>Web Services
                <small></small>
            </h1>

        </div>
        <div class="text-right">
            <h5 id="className">
                <span></span>
            </h5>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="list-group">
                <a href="javascript:void(0)" rel="get" class="list-group-item">Get Example</a>
                <a href="javascript:void(0)" rel="insert" class="list-group-item">Insert Example</a>
                <a href="javascript:void(0)" rel="update" class="list-group-item">Update Example</a>
                <a href="javascript:void(0)" rel="delete" class="list-group-item">Delete Example</a>
            </div>
        </div>

        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <section class="panel">
                        <header class="panel-heading">
                            JSON Results :
                        </header>
                        <div class="panel-body">
                                    <pre id="json_result" style="min-height: 300px;">

                                    </pre>
                        </div>
                        <div class="panel-footer">
                            <h5>Response Status : <span data-status="" class="label label-danger"></span></h5>
                        </div>
                    </section>


                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <form id="unit_test" method="post" action="main.php">

                        <div class="custom-field">

                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<footer class="text-center" style="margin-top: 40px;">
    <p class="muted">Copyright © 2015 - MSIT FRUIT</p>
</footer>
</body>
</html>