<?php
/**
 * Created by IntelliJ IDEA.
 * User: Sakda
 * Date: 3/12/2015
 * Time: 4:56 PM
 */

$config["get"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'get'
    ),
    'id' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 1
    )
);

$config["insert"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'insert'
    ),
    'name' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'Sakda'
    )
);

$config["update"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'update'
    ),
    'id' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 1
    ),
    'name' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 'Sakda'
    )
);

$config["delete"] = array(
    'method' => array(
        'type' => 'hidden',
        'placeholder' => '',
        'value' => 'delete'
    ),
    'id' => array(
        'type' => 'text',
        'placeholder' => '',
        'value' => 1
    )
);

$output = array(
    'field' => $config[$_REQUEST["method"]],
);
echo json_encode($output);