<?php
require_once('libraries/meekrodb.2.0.class.php');
require_once('libraries/helper.php');
require_once('config.php');
require_once('app-control.php');

$app = new App_control();
$return = array();
switch($_REQUEST["method"]){
    case"get_user":
        $return["result"] = $app->get_user();
        break;
    case"insert_user":
        $return["result"] = $app->insert_user();
        break;
    case"update_user":
        $return["result"] = $app->update_user();
        break;
    case"delete_user":
        $return["result"] = $app->delete_user();
        break;
    case"get_monitoring_config":
        $return["result"] = $app->get_monitoring_config();
        break;
    case"insert_monitoring_config":
        $return["result"] = $app->insert_monitoring_config();
        break;
    case"update_monitoring_config":
        $return["result"] = $app->update_monitoring_config();
        break;
    case"delete_monitoring_config":
        $return["result"] = $app->delete_monitoring_config();
        break;
    case"get_monitoring_schedule":
        $return["result"] = $app->get_monitoring_schedule();
        break;
    case"insert_monitoring_schedule":
        $return["result"] = $app->insert_monitoring_schedule();
        break;
    case"update_monitoring_schedule":
        $return["result"] = $app->update_monitoring_schedule();
        break;
    case"delete_monitoring_schedule":
        $return["result"] = $app->delete_monitoring_schedule();
        break;
    case"get_monitoring":
        $return["result"] = $app->get_monitoring();
        break;
    default:
        $return["error"] = array("code"=>404,"message"=>"Method not found.");
        break;
}
echo json_encode($return);
?>