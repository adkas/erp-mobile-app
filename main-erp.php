<?php
require_once('libraries/meekrodb.2.0.class.php');
require_once('libraries/helper.php');
require_once('config.php');
require_once('erp-control.php');

$erp = new Erp_control();
$return = array();

switch($_REQUEST["method"]){
	case"get_user":
		$return["result"] = $erp->get_user();
	break;
	case"get_manufacture_value":
		$return = $erp->get_manufacture_value();
		break;
	case"get_material_remain":
		$return = $erp->get_material_remain();
		break;
	case"get_product_boiling_qty":
		$return = $erp->get_product_boiling_qty();
		break;
	case"get_product_cleaning_qty":
		$return = $erp->get_product_cleaning_qty();
		break;
	case"get_product_cutting_qty":
		$return = $erp->get_product_cutting_qty();
		break;
	case"get_product_packing_qty":
		$return = $erp->get_product_packing_qty();
		break;
	case"get_product_putting_qty":
		$return = $erp->get_product_putting_qty();
		break;
	case"get_product_remain":
		$return = $erp->get_product_remain();
		break;
	case"get_purchase_value":
		$return = $erp->get_purchase_value();
		break;
	case"insert_purchase":
		$return = $erp->insert_purchase();
		break;
	default:
		$return["error"] = array("code"=>404,"message"=>"Method not found.");
	break;
}
echo json_encode($return);
?>