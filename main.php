<?php
require_once('libraries/meekrodb.2.0.class.php');
require_once('libraries/helper.php');
require_once('libraries/formvalidator.php');
require_once('config.php');
require_once('control.php');
$control = new Control();
$return = array();
switch($_REQUEST["method"]){
    case"get":
        $return["result"] = $control->get();
        break;
    case"insert":
        $return["result"] = $control->insert();
        break;
    case"update":
        $return["result"] = $control->update();
        break;
    case"delete":
        $return["result"] = $control->delete();
        break;
    default:
        $return["error"] = array("code"=>404,"message"=>"Method not found.");
        break;
}
echo json_encode($return);
?>