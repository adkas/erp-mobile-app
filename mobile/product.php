<!DOCTYPE HTML>

<html>

<head><title>ERP - Monitoring</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- FontAwesome - http://fortawesome.github.io/Font-Awesome/ -->

    <link rel="stylesheet" href="css/font-awesome.min.css"/>

    <!-- jQueryMobileCSS - original without styling -->

    <link rel="stylesheet" href="css/jquery.mobile-1.4.2.min.css"/>

    <!-- nativeDroid core CSS -->

    <link rel="stylesheet" href="css/jquerymobile.nativedroid.css"/>

    <!-- nativeDroid: Light/Dark -->

    <link rel="stylesheet" href="css/jquerymobile.nativedroid.dark.css" id='jQMnDTheme'/>

    <!-- nativeDroid: Color Schemes -->

    <link rel="stylesheet" href="css/jquerymobile.nativedroid.color.green.css" id='jQMnDColor'/>



    <link rel="stylesheet" href="css/style.css"/>

    <!-- jQuery / jQueryMobile Scripts -->

    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="js/jquery.mobile-1.4.2.min.js"></script>

</head>

<body>



<div id='map-canvas'></div>



<div data-role="page" data-theme='b'>





    <div data-role="header" data-position="fixed" data-tap-toggle="false" data-theme='b'>

        <a href="home.php" data-ajax="false"><i class='fa fa-bars'></i></a>

        <h1>Product</h1>

    </div>



    <div data-role="content">





        <ul data-nativedroid-plugin='cards'>


            <li data-cards-type='sports'>
                <h1 style="font-weight: bold; text-decoration: underline;">Rambutan in Syrup</h1>
                <h1>Quantity<span>1,200</span></h1>

                <h1>Unit<span>cans</span></h1>

                <h2>Last Updated : April 23,2015 09.00 AM</h2>

            </li>

            <li data-cards-type='text'>

                <h1 style="font-weight: bold; text-decoration: underline;">Contact Info</h1>

                <h1>Arsene Wenger</h1>

                <p>Tel. <span style="color: #99cc00;">02-5555555</span></p>

            </li>

        </ul>


    </div>



</div>



<script src="js/nativedroid.script.js"></script>

</body>

</html>

