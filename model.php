<?php
class Model
{
    function __construct($DB_USER, $DB_PASSWORD, $DB_NAME, $DB_SERVER)
    {
        DB::$user = $DB_USER;
        DB::$password = $DB_PASSWORD;
        DB::$dbName = $DB_NAME;
        DB::$host = $DB_SERVER;
        DB::query("SET NAMES UTF8");
    }

    function insert($table,$data = array())
    {
        DB::insert($table, $data);
        $id = DB::insertId();
        $result = DB::query("SELECT * FROM test WHERE id=".$id);
        return $result;
    }

    function get_example($id){
        $result = DB::query("SELECT * FROM test WHERE id=".$id);
        return $result;
    }

    function update($params = array())
    {
        $table = $params["table"];
        $data = $params["data"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::update($table, $data, $where, $id);
        $result = DB::query("SELECT * FROM test WHERE id=".$id);
        return $result;
    }

    function query($sql){
        $query = DB::query($sql);
        return $query;
    }

    function delete($params = array()){
        $table = $params["table"];
        $where = $params["where"]; //column=%s
        $id = $params["id"];
        DB::delete($table, $where,$id);
    }


}

?>