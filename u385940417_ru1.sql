
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 21, 2015 at 02:41 PM
-- Server version: 5.1.71
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u385940417_ru1`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('fef98721c84ef1b6796f852c6afc259d', '27.130.201.147', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36', 1426948547, 'a:4:{s:9:"user_data";s:0:"";s:8:"log_u_id";s:1:"1";s:10:"log_u_name";s:14:"Sakda Srijumpa";s:11:"log_u_email";s:20:"welovelink@gmail.com";}'),
('b8b86ee7b15526d11ac42acf420fa7a6', '27.130.201.147', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36', 1426947326, 'a:4:{s:9:"user_data";s:0:"";s:8:"log_u_id";s:1:"1";s:10:"log_u_name";s:14:"Sakda Srijumpa";s:11:"log_u_email";s:20:"welovelink@gmail.com";}');

-- --------------------------------------------------------

--
-- Table structure for table `manufacture_list`
--

CREATE TABLE IF NOT EXISTS `manufacture_list` (
  `manufacture_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `step_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `manufacture_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`manufacture_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `manufacture_list`
--

INSERT INTO `manufacture_list` (`manufacture_id`, `product_id`, `step_id`, `quantity`, `unit`, `manufacture_date`) VALUES
(2, 1, 1, 370, 'kgs', '2015-03-12 14:18:35'),
(3, 1, 2, 400, 'kgs', '2015-03-21 09:47:37'),
(4, 1, 4, 300, 'kgs', '2015-03-21 09:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `material_list`
--

CREATE TABLE IF NOT EXISTS `material_list` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`material_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `material_list`
--

INSERT INTO `material_list` (`material_id`, `material_name`, `quantity`, `unit`, `updated_date`) VALUES
(1, 'Rambutan', 800, 'kgs', '2015-03-21 14:33:33'),
(2, 'Syrup', 4000, 'cc', '2015-03-21 14:34:05'),
(3, 'Preservative', 200, 'cc', '2015-03-21 14:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `monitoring`
--

CREATE TABLE IF NOT EXISTS `monitoring` (
  `monitor_id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_name` varchar(50) NOT NULL,
  PRIMARY KEY (`monitor_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `monitoring`
--

INSERT INTO `monitoring` (`monitor_id`, `monitor_name`) VALUES
(1, 'Sale Order'),
(2, 'Menufacturing'),
(3, 'Material'),
(4, 'Product');

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_config`
--

CREATE TABLE IF NOT EXISTS `monitoring_config` (
  `config_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `monitor_id` int(11) NOT NULL,
  `expect_value` int(11) NOT NULL,
  `actual_value` int(11) NOT NULL,
  `yellow_value` int(11) NOT NULL,
  `red_value` int(11) NOT NULL,
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `alarm_mp3` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_material`
--

CREATE TABLE IF NOT EXISTS `monitoring_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitoring_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_menufacture`
--

CREATE TABLE IF NOT EXISTS `monitoring_menufacture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitoring_id` int(11) NOT NULL,
  `step_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `monitoring_menufacture`
--

INSERT INTO `monitoring_menufacture` (`id`, `monitoring_id`, `step_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 2, 4),
(5, 2, 5),
(6, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_product`
--

CREATE TABLE IF NOT EXISTS `monitoring_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitoring_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_product_config`
--

CREATE TABLE IF NOT EXISTS `monitoring_product_config` (
  `config_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `monitor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `expect_value` int(11) NOT NULL,
  `actual_value` int(11) NOT NULL,
  `yellow_value` int(11) NOT NULL,
  `red_value` int(11) NOT NULL,
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_purchase`
--

CREATE TABLE IF NOT EXISTS `monitoring_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitoring_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_purchase_config`
--

CREATE TABLE IF NOT EXISTS `monitoring_purchase_config` (
  `config_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `monitor_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `expect_value` int(11) NOT NULL,
  `actual_value` int(11) NOT NULL,
  `yellow_value` int(11) NOT NULL,
  `red_value` int(11) NOT NULL,
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_schedule`
--

CREATE TABLE IF NOT EXISTS `monitoring_schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `from_time` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `to_time` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=ยังไม่ผ่าน,1=ผ่าน',
  PRIMARY KEY (`schedule_id`),
  UNIQUE KEY `schedule_id` (`schedule_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `monitoring_schedule`
--

INSERT INTO `monitoring_schedule` (`schedule_id`, `uid`, `from_time`, `to_time`, `status`) VALUES
(1, 2, '2015-', '2015-', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_list`
--

CREATE TABLE IF NOT EXISTS `product_list` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product_list`
--

INSERT INTO `product_list` (`product_id`, `product_name`, `quantity`, `unit`, `updated_date`) VALUES
(1, 'Rambutan in Syrup', 500, 'cans', '2015-03-21 14:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_list`
--

CREATE TABLE IF NOT EXISTS `purchase_list` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `unit` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`purchase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `step`
--

CREATE TABLE IF NOT EXISTS `step` (
  `step_id` int(11) NOT NULL AUTO_INCREMENT,
  `step_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `step_detail` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`step_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `step`
--

INSERT INTO `step` (`step_id`, `step_name`, `step_detail`) VALUES
(1, 'Cleaning', 'ทำความสะอาด'),
(2, 'Peeling', 'ปอกเปลือก'),
(3, 'Filling', 'การบรรจุ (filling) อาหารที่บรรจุ อาจเป็นของเหลวอย่างเดียว เช่น น้ำผลไม้ ของแข็ง เช่น ชิ้นผลไม้ ชิ้นเนื้อ ผสมกับส่วนที่เป็นของเหลวเช่น น้ำเชื่อม น้ำเกลือ น้ำมัน ซ๊อส ในการบรรจุจะต้องบรรจุให้อาหารมีปริมาตร น้ำหนักบรรจุ (ไม่บรรจุจนเต็มพอดี เพราะระหว่างการให้ความร้อนจะมีการขยายตัวของของเหลว'),
(4, 'Hermitically Seal', 'ปิดผนึกสนิท (hermitically seal) โดยใช้ hermectically sealed container เพื่อป้องกันไม่ให้จุลินทรีย์ และสิ่งปนเปื้อนจากภายนอก รวมทั้ง ไอน้ำ อากาศ ผ่าน เข้าไปภายในบรรจุภัณฑ์ ภายหลังการฆ่าเชื้อแล้ว การปิดฝากระป๋องใช้เครื่องปิดฝากระป๋อง (double seam)'),
(5, 'Thermal Processing', 'การฆ่าเชื้อด้วยความร้อน (thermal processing) โดยการใช้ความร้อนเพื่อทำลายจุลินทรีย์ ให้เพียงพอกับการฆ่าเชื้อระดับการค้า (commercial sterilization) ตามอุณหภูมิและระยะเวลาที่กำหนด (schedule process) การฆ่าเชื้อด้วยความร้อนที่อุณภูมิสูงกว่า 100 องศาเซลเซียส จะทำใน retort ซึ่งเป็นหม้อฆ่าเชื้อภายใต้ความดันสูง หากฆ่าเชื้อที่อุณหภูมิ100 องศาเซลเซียส หรือต่ำกว่า สามารถค่าเชื้อในหม้อต้ม (cooker) ที่ความดันบรรยกาศปกติใด้ เพื่อให้อาหารปลอดภัยและเก็บรักษาไว้ได้ในระยะเวลาที่กำหนด'),
(6, 'Packing', 'การบรรจุใส่ลังขนาดใหญ่');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `email`, `password`, `firstname`, `lastname`, `position`, `department`) VALUES
(1, 'welovelink@gmail.com', '7627cb9027e713e301e83a8f13057055', 'Sakda', 'Srijumpa', 'Programmer', 'IT'),
(2, 'mie@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'kittipon', 'kongsom', 'manager', 'manufacture'),
(3, 'top@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Chatchawan', 'Nutwong', 'Salaryman', 'IT');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
